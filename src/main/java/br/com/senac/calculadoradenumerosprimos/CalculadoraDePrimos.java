
package br.com.senac.calculadoradenumerosprimos;


public class CalculadoraDePrimos {
    
    //997
    //998
    public boolean isPrimo(int numero){
        
        int divisoes = 0 ; 
        
        for(int i = 1 ; i <= numero; i++){
        
            if(numero%i == 0 ){
                divisoes++;
                if(divisoes > 2 ){
                    break;
                }
            }
        }
        
        return divisoes == 2  ;
    }
    
}
